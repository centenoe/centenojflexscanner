Name: Edgar Centeno
Project: Jflex Scanner

This program uses Jflex to identify numbers, letters, words, and symbols (" - * / + ").
If you put a 0 (zero) at the start of a number sequence such as 012, it will count that 
0 as an illegal character. The second iteration is a non-standalone Jflex program.
The second iteration will also now ignore comments. The third iteration now identifies 
KeyWords and Symbols in a file. 

This folder includes these files:
.gitignore
lex.flex
MyScanner.java
test.txt
src folder

The src folder includes the second and third iteration of the Jflex Scanner project
The src folder includes:
Main.java
Token.java
TokenType.java
LookupTable.java
lext.flex

For the first folder:
To run the program, make sure that MyScanner.class is inside the same folder as the text document 
you want to test. In the command prompt type in "java MyScanner test.txt" 
or "java MyScanner [inputfile]" if you have your test document.

If you wish to change the lex.flex file, please make sure to recompile the file 
to create a new MyScanner.java file. Javac the MyScanner.java to compile it 
then type in "java MyScanner [inputfile]"

For the src folder:
to run the program please compile lext.flex file by typing "jflex lext.flex" in the command line
MyScannerTwo should appear on you folder.
Type "javac Main.java" to create class files for all the programs inside the src folder.
A class file for every file should appear in src folder.
Type "java Main workingtest.txt" to test the .txt file that works
Type "java Main badtest.txt" to test the .txt file that should have illegal characters




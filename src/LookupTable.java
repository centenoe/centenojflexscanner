import java.util.HashMap;

/**
 * The LookupTable class has a list of C-like keywords.
 */
public class LookupTable {

    /**
     * Here are the list of keywords for the C-like language
     */
    public static final HashMap<String, TokenType> TABLE = new HashMap<>(){

        {
        put("char", TokenType.CHAR);
        put("int", TokenType.INT);
        put("float", TokenType.FLOAT);
        put("if", TokenType.IF);
        put("else", TokenType.ELSE);
        put("while", TokenType.WHILE);
        put("print", TokenType.PRINT);
        put("read", TokenType.READ);
        put("return", TokenType.RETURN);
        put("func", TokenType.FUNC);
        put("program", TokenType.PROGRAM);
        put("end", TokenType.END);
        }
    };
}

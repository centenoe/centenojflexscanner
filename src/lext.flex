/**
 * Edgar Centeno
 * This is the jflex Scanner Project for Iteration 2 and 3
 *this will create the MyScannerTwo.java file once jflex has ran.
 *lex file is standalone
 *lext file is not standalone and uses tokens
 */

/* Declarations */
%%

%class  MyScannerTwo   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
/* Patterns */

other         = .
/* singular letters*/
letter        = [a-zA-z]
/*A leading number zero will never count as a number but zero itself works*/
number        = [1-9][0-9]+|0

/* these are the symbols*/
semiColon         = ";"
leftParenthese    = ")"
rightParenthese   = "("
rightCurlyBracket = "{"
leftCurlyBracket  = "}"
leftBracket       = "]"
rightBracket      = "["


add               = \+
subtract          = \-
multiply          = \*
divide            = \/

equal             = "="
less              = "<"
greater           = ">"
lessEqual         = "<""="
greaterEqual      = ">""="
notEqual          = "!""="

and               = "&""&"
or                = "|""|"
not               = "!"

/* if it is a word check if it is in the keyword list*/
ID                = {letter}+

/*comment will ignore comments and white spaces in a file */
comment       = "/*" [^"/*"] ~"*/"
whitespace    = [ \n\t]

%%
/* Lexical Rules */

              /* If the word is in the LookupTable then Idenity the keyword
                 else return it as a ID*/
{ID}               {
                     if(LookupTable.TABLE.containsKey(yytext()))
                        return (new Token (yytext(), LookupTable.TABLE.get( yytext())));
                        else
                        return (new Token(yytext(), TokenType.ID));
                    }

{number}            {
              /** Number has been found printout */
                      return( new Token( yytext(), TokenType.NUMBER));
                    }

                      /** Below is the printout of all the symbols */

{semiColon}         {

                      return( new Token( yytext(), TokenType.SEMI_COLON));
                    }

{leftParenthese}    {

                      return( new Token( yytext(), tOKENtYPE.LEFT_PARENTHESE));
                    }

{rightParenthese}   {
                      return( new Token( yytext(), TokenType.RIGHT_PARENTHESE));
                    }

{rightCurlyBracket} {
                      return( new Token( yytext(), TokenType.RIGHT_C_BRACKET));
                    }

{leftCurlyBracket}  {
                      return( new Token( yytext(), TokenType.LEFT_C_BRACKET));
                    }

{leftBracket}       {
                      return( new Token( yytext(), TokenType.LEFT_BRACKET));
                    }

{rightBracket}      {
                      return( new Token( yytext(), TokenType.RIGHT_BRACKET));
                    }

{add}               {
                      return( new Token( yytext(), TokenType.ADD));
                    }

{subtract}          {
                      return( new Token( yytext(), TokenType.SUBTRACT));
                    }

{multiply}          {
                      return( new Token( yytext(), TokenType.MULTIPLY));
                    }

{divide}            {
                      return( new Token( yytext(), TokenType.DIVIDE));
                    }

{equal}             {
                      return( new Token( yytext(), TokenType.EQUAL));
                    }

{less}              {
                      return( new Token( yytext(), TokenType.LESS));
                    }

{greater}           {
                      return( new Token( yytext(), TokenType.GREATER));
                    }

{lessEqual}         {
                      return( new Token( yytext(), TokenType.LESS_EQUAL));
                    }

{greaterEqual}      {
                      return( new Token( yytext(), TokenType.GREATER_EQUAL));
                    }

{notEqual}          {
                      return( new Token( yytext(), TokenType.NOT_EQUAL));
                    }

{and}               {
                      return( new Token( yytext(), TokenType.AND));
                    }

{or}                {
                      return( new Token( yytext(), TokenType.OR));
                    }

{not}               {
                      return( new Token( yytext(), TokenType.NOT));
                    }

{comment}     {  /* Do nothing with comments*/}
{whitespace}  {  /* Do nothing with whitespaces*/}

{other}    {
            /** illegal char printout */
             System.out.println("Illegal char: '" + yytext() + "' found.");
           }

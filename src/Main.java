
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * this main will test the jflex program (MyScannerTwo)
 */
public class Main
{
    /**
     * The main that starts the application
     */
    public static void main( String[] args) {
        // read the file
        String filename = args[0];
        FileInputStream fis = null;
        try {
            // create input stream on the file given
            fis = new FileInputStream(filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //create a stream reader
        InputStreamReader isr = new InputStreamReader(fis);
        MyScannerTwo scanner = new MyScannerTwo(isr);
        Token aToken = null;
        do // as long as there is still a token in the scanner keep going
        {
            try {
                // get the token
                aToken = scanner.nextToken();
            }

            catch( Exception e) { e.printStackTrace();}
            // if there is a token then show it
            if( aToken != null)
                System.out.println(aToken.toString());
        } while( aToken != null);

    }
}
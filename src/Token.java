/**
 *  A simple Token class containing only a String.
 */
public class Token
{
    private String contents;
    private TokenType type;

    //a Token must have a String input and a TokenType
    public Token( String input, TokenType type)
    {
        this.contents = input;
        this.type = type;
    }

    public String getLexeme() {
        return this.contents;
    }

    //get the type
    public TokenType getTokenType() {
        return this.type;
    }

    //return the Token and the Type of token
    @Override
    public String toString() { return "Token: " + this.contents + "  Type: " + this.type;}
}
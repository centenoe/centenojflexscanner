/**
 * Edgar Centeno
 * This is the jflex Scanner Project
 */

/* Declarations */
%%

%class  MyScannerTwo   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   String      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
/* Patterns */

other         = .
letter        = [a-zA-z]
word          = {letter}+
number        = [1-9][0-9]*
symbol        = [\+\-\*\/]
whitespace    = [ \n\t]

%%
/* Lexical Rules */
{letter}    {
              /** letter has been found printout */
              System.out.println ("found a letter: " + yytext());
            }

{word}     {
             /** Word has been found printout */
             System.out.println("Found a word: " + yytext());
            }

{number}    {
              /** Number has been found printout */
              System.out.println ("found a number: " + yytext());
            }

{symbol}    {
              /** symbol has been found printout */
              System.out.println ("found a symbol: " + yytext());
            }

{whitespace}  {  /* Do nothing with whitespaces*/
                 return "";
              }

{other}    {
            /** illegal char printout */
             System.out.println("Illegal char: '" + yytext() + "' found.");
           }
